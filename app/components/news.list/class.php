<?php

class NewsList extends Component
{
    public function _construct($name, $template, $params)
    {
        parent::__construct($name, $template, $params);
    }

    private function prepareParams()
    {
        if (!isset($this->params['count'])) {
            $this->params['count'] = 1;
        }
        if (!isset($this->params['show_pic'])) {
            $this->params['show_pic'] = 'Y';
        }
        if (!isset($this->params['data'])) {
            $this->params['data'] = '';
        }
    }

    private function setResult($array)
    {
        $this->arrResult = $array;
    }

    private function getResult($pageNumber, &$countElements)
    {
        $arrResult = array();
        if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/app/data/' . $this->params['data'])) {
            $xml = simplexml_load_file($_SERVER['DOCUMENT_ROOT'] . '/app/data/' . $this->params['data']);
        } else {
           return false;
        }

        $countElements = count($xml);
        $limit = $pageNumber * $this->params['count'];
        for ($i = ($pageNumber * $this->params['count'] - $this->params['count']); $i < $limit; $i++) {
            if (!isset($xml->item[$i]->title)) break;
            $arrResult[$i]['item'] = '';
            $arrResult[$i]['title'] = $xml->item[$i]->title;
            $arrResult[$i]['link'] = $xml->item[$i]->link;
            $arrResult[$i]['description'] = $xml->item[$i]->description;
            $arrResult[$i]['picture'] = $xml->item[$i]->picture;
            $arrResult[$i]['code'] = $xml->item[$i]->code;
        }
        return $arrResult;
    }

    public function executeComponent()
    {
        $countElements = 0;
        $this->prepareParams();
        $arr = $this->getResult($this->getPage(), $countElements);
        if(!$arr){
            $application = Application::getInstance();
            $application->set404();
            return false;
        }
        $this->setH1();
        $this->setResult($arr);
        $this->includeTemplate('template');
        $this->showPagination($countElements);
    }

    public function setH1() {
        $application = Application::getInstance();
        $application->setPageProperty('h1', 'Новости');
    }

    private function showPagination($countElements)
    {
        $page = $this->getPage();
        $last = ceil(($countElements / $this->params['count']));
        echo "<div id='pagination'>";
        echo "<ul id=\"pagination-flickr\">";
        if ($page > 2) {
            ?>
            <li><a href="/<?= $this->template ?>/"><?= 1 ?></a></li>
            <?php
        }
        if ($page > 3) {
            ?>

            <li>
                <a href="/<?= $this->template ?>/page-<?= ceil((($page - 1) / 2)) ?>/">...</a>
            </li>

            <?php
        }
        if ($page - 1 != 0) {
            if ($page - 1 != 1) {
                ?>
                <li><a href="/<?= $this->template ?>/page-<?= $page - 1 ?>/"><?= $page - 1 ?></a></li>
                <?php
            } else { ?>
                <li><a href="/<?= $this->template ?>/"><?= 1 ?></a></li>
                <?php
            }
        }
        ?>

        <li class="active"><?= ($this->getPage()) ?></li>

        <?php
        if ($last != $page) {
            ?>

            <li><a href="/<?= $this->template ?>/page-<?= $page + 1 ?>/"><?= $page + 1 ?></a></li>

            <?php
        }
        if ($last - $page > 2) {
            ?>

            <li>
                <a href="/<?= $this->template ?>/page-<?= ceil(($page + $last) / 2) ?>/">...</a>
            </li>

            <?php
        }
        if ((int)($last - 1) != $page && (int)($last) != $page) {
            ?>

            <li>
                <a href="/<?= $this->template ?>/page-<?= $last ?>/"><?= $last ?></a>
            </li>
            <?php
        }
        echo "</ul>";
        echo "</div>";
    }

}
