<?php

class News extends Component
{
    public function __construct($name, $template, $params)
    {
        parent::__construct($name, $template, $params);
    }

    public function executeComponent()
    {
        $code = $this->getCode();
        $this->arrResult['code'] = $code;
        $this->includeTemplate( $this->getPage());
    }

    public function getPage()
    {
        $queryString = $_SERVER['REQUEST_URI'];
        $url = explode("/", $queryString);
        if (count($url) > 3 && !stristr($url[2], 'page'))
            return 'detail';
        return 'list';

    }

    public function getCode()
    {
        $queryString = $_SERVER['REQUEST_URI'];
        $url = explode("/", $queryString);
        if (count($url) > 3 && !stristr($url[2], 'page')) {
            return $url[2];
        }
        return false;
    }
}