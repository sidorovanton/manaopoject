<?php

class NewsDetail extends Component
{
    public function executeComponent()
    {
        if (!$result = $this->getByCode($this->params['code'])) {
            Application::getInstance()->set404();
            return false;
        }
        $this->arrResult = $result;
        $this->includeTemplate();
        $this->setH1();
    }

    public function getByCode($code)
    {
        $arrResult = array();
        if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/app/data/' . $this->params['data'])) {
            $xml = simplexml_load_file($_SERVER['DOCUMENT_ROOT'] . '/app/data/' . $this->params['data']);
            for ($i = 0; $i < count($xml); $i++) {
                if ($xml->item[$i]->code == $code) {
                    $arrResult['title'] = $xml->item[$i]->title;
                    $arrResult['link'] = $xml->item[$i]->link;
                    $arrResult['description'] = $xml->item[$i]->description;
                    $arrResult['picture'] = $xml->item[$i]->picture;
                    $arrResult['code'] = $xml->item[$i]->code;
                    break;
                }
            }
            return $arrResult;
        } else
            return false;
    }

    public function setH1()
    {
        Application::getInstance()->setPageProperty("h1", $this->arrResult['title']);
    }
}
