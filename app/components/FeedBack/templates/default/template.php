
<div class="blockForm">
    <form class="contact_form" action="" method="POST" name="contact_form">
        <ul>
            <li>
                <h4>Ваше имя</h4>
                <input name="name" type="text" placeholder="Иванов Иван"/>
            </li>
            <li>
                <h4>Телефон</h4>
                <input name="phone" type="phone" placeholder="+375-29-123-45-67"/>
            </li>
            <li>
                <h4>Тема</h4>
                <select size="1" name="theme">
                    <option disabled>Выберите героя</option>
                    <option selected value="Чебурашка">Чебурашка</option>
                    <option  value="Крокодил Гена">Крокодил Гена</option>
                    <option value="Шапокляк">Шапокляк</option>
                    <option value="Крыса Лариса">Крыса Лариса</option>
                </select>
            </li>
            <li>
                <h4>Текст</h4>
                <textarea name="message" cols="40" rows="6" placeholder="Ваше сообщение"></textarea>
            </li>
            <button class="submit" type="submit">Отправить</button>
        </ul>
    </form>
</div>