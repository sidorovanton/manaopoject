<?php

class FeedBack extends Component
{
    public function __construct($name, $template, $params)
    {
        parent::__construct($name, $template, $params);
    }

    function executeComponent()
    {
        $this->includeTemplate();
        if ($this->validate()) {
//            var_dump($this->params);
            $this->send();
//            Application::getInstance()->restartBuffer();
//            $url = $_SERVER["PHP_SELF"];
//            header("Location: ".$url);
        }
    }

    private function getPostParams()
    {
        $res = array();
        if (isset($_POST['name'])) {
            $res['subject'] = $_POST['theme'];
            $res['fields']['name'] = $_POST['name'];
            $res['fields']['phone'] = $_POST['phone'];
            $res['fields']['message'] = $_POST['message'];
            $res['template'] = 'mail';
        }
        return $res;
    }

    public function validate()
    {
        $this->params = $this->getPostParams();
        if (count($this->params) == 0 || strlen($this->params['fields']['name']) < 2 ||
            strlen($this->params['fields']['phone']) < 1 ||
            empty($this->params['subject']) ||
            strlen($this->params['fields']['message']) < 3
        ) {
            return false;
        }
        return true;
    }

    public function send()
    {
        Application::getInstance()->send($this->params);
    }

}