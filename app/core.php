<?php

final class Application
{
    private $__components = array();
    private $template = '';
    public $status = 200;
    private $property = array();
    private $config = array();
    private $db = null;
    private static $instance = null;

    public static function getInstance()
    {
        if (self::$instance == null)
            self::$instance = new static();
        return self::$instance;
    }

    public function &getDB()
    {
        return $this->db;
    }

    public function setTemplate($template = 'default')
    {
        if (empty($this->template)) {
            $this->template = $template;
        }
    }

    public function includeFile($path)
    {
        $fullPath = $_SERVER['DOCUMENT_ROOT'] . $path;
        if (file_exists($fullPath)) {
            include_once $fullPath;
        }
    }

    private function __construct()
    {
        include_once 'config.php';
        $this->config = $config;
        $this->db = DB::getInstance();
    }

    private function __clone()
    {
    }

    private function __wakeup()
    {
    }

    private function __sleep()
    {
    }

    public function getTemplate()
    {
        return $this->template;
    }

    public function getTemplatePath()
    {
        return "/app/templates/" . $this->getTemplate() . "/";
    }

    public function includeComponent($name, $template = 'default', $params = array())
    {
        if (isset($this->__components[$name])) {
            $class = $this->__components[$name];
            $obj = new $class($name, $template, $params);
        } else {
            $classes = get_declared_classes();
            $this->includeFile("/app/components/" . $name . "/class.php");
            $diff = array_diff(get_declared_classes(), $classes);

            for ($i = 0; $i <= count($classes); $i++) {
                if (isset($diff[$i])) {
                    if (is_subclass_of($diff[$i], 'Component')) {
                        $this->__components[$name] = $diff[$i];
                        break;
                    }
                }

            }
            $class = $this->__components[$name];
            $obj = new $class($name, $template, $params);
        }
        $obj->executeComponent();
    }


    public function restartBuffer()
    {
        ob_clean();
    }

    private function getBuffer()
    {
        return ob_get_clean();
    }

    public function setPageProperty($id, $value)
    {
        $this->property[$id] = $value;
    }

    private function showEditedContent()
    {
        $buffer = $this->getBuffer();
        $result = str_replace($this->getReplaceProperties(), $this->property, $buffer);
//        $result = preg_replace('(#.*#)', '', $result);
        echo $result;
    }

    private function getReplaceProperties()
    {
        $array = array();
        foreach ($this->property as $key => $value) {
            $array[] = $this->getProperty($key);
        }
        return $array;
    }


    public function getPageProperty($id)
    {
        return $this->property[$id];
    }

    public function showProperty($id)
    {
        $this->property[$id] = '';

        echo $this->getProperty($id);
    }

    private function getProperty($id)
    {
        return '#PAGE_PROPERTY_' . $id . '#';
    }

    public function showHeader()
    {
        $this->handler('onProlog');
        ob_start();
        $path = $this->getTemplatePath() . "header.php";
        $this->includeFile($path);
    }

    public function showFooter()
    {
        $path = $this->getTemplatePath() . "footer.php";
        $this->includeFile($path);
        $this->handler('onEpilog');
        $this->showEditedContent();
    }

    public function handler($event)
    {
        if (function_exists($event)) {
            call_user_func($event);
        }
    }

    public function translit($str)
    {
        $str = strtolower($str);

        $letArray = array(
            "а" => "a",
            "б" => "b",
            "в" => "v",
            "г" => "g",
            "д" => "d",
            "е" => "e",
            "ё" => "yo",
            "ж" => "zh",
            "з" => "z",
            "и" => "i",
            "й" => "i",
            "к" => "k",
            "л" => "l",
            "м" => "m",
            "н" => "n",
            "о" => "o",
            "п" => "p",
            "р" => "r",
            "с" => "s",
            "т" => "t",
            "у" => "u",
            "ф" => "f",
            "х" => "h",
            "ц" => "c",
            "ч" => "ch",
            "ш" => "sh",
            "щ" => "sch",
            "ъ" => "",
            "ы" => "y",
            "ь" => "",
            "э" => "ye",
            "ю" => "u",
            "я" => "ya",
        );
        $translit = str_replace(array_keys($letArray), $letArray, $str);

        $symb = array(' — ', '/', '"', '*', '-', '+', '@', '#', '№', '$', '%', '^', '&', '~', '`', '=', ' ');
        $symbBug = array('[', ']', '{', '}', '<', '>', '(', ')', '„', '“', '»', '«', '!', '?', '.', ',', ':', ';',
            'nbsp');
        $symbResult = str_replace($symb, '-', $translit);

        $symbBugResult = str_replace($symbBug, '', $symbResult);

        return $symbBugResult;
    }

    public function checkEmail($email)
    {
        $regex = '/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/';
        if (preg_match($regex, $email)) {
            return true;
        }
        return false;
    }

    public function send($data)
    {
        if (!file_exists($_SERVER['DOCUMENT_ROOT'] . '/app/mail/templates/' . $data['template'] . '.html')) {
            $data['template'] = $this->config['template'];
        }

        if (!$this->checkEmail($data['email'])) {
            $data['email'] = $this->config['email'];
        }
        if (empty($data['subject'])) {
            return false;
        }

        $mail = new Mail();
        $mail->send($data);
    }

    public function set404()
    {
        $this->restartBuffer();
        header("HTTP/1.0 404 Not Found");
        $this->includeFile('/404.php');
    }

}

abstract class Component
{
    protected $arrResult = array();
    private $name = '';
    protected $template = '';
    protected $params = array();

    public function __construct($name, $template, $params)
    {
        $this->name = $name;
        $this->template = $template;
        $this->params = $params;
    }

    final function includeTemplate($page = "template")
    {
        $arResult = $this->arrResult;
        $arParams = $this->params;
        $fullPath = $_SERVER['DOCUMENT_ROOT'] . "/app/components/" . $this->name . "/templates/" . $this->template . "/" . $page . ".php";
        if (file_exists($fullPath)) {
            include_once $fullPath;
        }
    }

    public function getPage()
    {
        if (isset($_GET['PAGE']) && intval($_GET['PAGE'])) {
            return $_GET['PAGE'];
        }
        return 1;
    }

    abstract function executeComponent();
}


class Mail
{
    public function send($data)
    {
        $headers = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
        return mail($data['email'], $data['subject'], $this->getMail($data), $headers);
    }

    public function getTemplate($id)
    {
        return file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/app/mail/templates/' . $id . '.html');
    }

    public function getMail($data)
    {
        $ar = array();
        $text = $this->getTemplate('mail');

        foreach ($data['fields'] as $key => $value) {
            $ar[] = '#' . strtoupper($key) . '#';
        }

        return str_replace($ar, $data['fields'], $text);
    }
}


class DB
{
    private $tables = array();
    private static $db = null;

    public static function getInstance()
    {
        if (self::$db == null)
            self::$db = new static();
        return self::$db;
    }

    private function __wakeup()
    {
        // TODO: Implement __wakeup() method.
    }

    private function __sleep()
    {
        // TODO: Implement __sleep() method.
    }

    private function __invoke()
    {
        // TODO: Implement __invoke() method.
    }

    private function __clone()
    {
        // TODO: Implement __clone() method.
    }

    private function __construct()
    {

    }

    public function &getTable($name)
    {
        if (empty($this->tables[$name])) {
            $path = $_SERVER['DOCUMENT_ROOT'] . '/app/data/' . $name . '.xml';
            if (file_exists($path)) {
                $this->tables[$name] = new DBTable($path);
            } else {
                Application::getInstance()->restartBuffer();
                throw new Exception('File not found ' . $path);

            }
        }
        return $this->tables[$name];
    }
}


class DBTable
{
    public $table = array();
    public $map = array();
    private $path = '';

    public function __construct($path)
    {
        $this->createTable($path);
    }

    public function __destruct()
    {
        $string = "<?xml version='1.0' encoding='UTF-8'?>\n<table>\n<columns>\n";

        foreach ($this->map as $key => $value) {
            $string .= "\t<col type='" . $value . "'>" . $key . "</col>\n";
        }
        $string .= "</columns>\n<rows>";

        foreach ($this->table as $value) {
            $string .= "\n\t<row>\n";
            foreach ($this->map as $key => $val) {
                $string .= "\t\t<" . $key . ">" . $value[$key] . "</" . $key . ">\n";
            }
            $string .= "\t</row>";
        }

        $string .= "\n</rows>\n</table>";

        $file = fopen($this->path, "w");
        fwrite($file, $string);
        fclose($file);
    }

    private function createTable($path)
    {
        $this->path = $path;
        $content = simplexml_load_file($path);
        $this->table = $this->setTable($content);
        $this->map = $this->setMap($content);
    }

    private function setTable($xmlObj)
    {
        $table = array();
        foreach ($xmlObj->rows->row as $item) {
            foreach ($item->children() as $key => $value) {
                $table[(string)$item->id][$key] = (string)$value;
            }
        }
        return $table;
    }

    private function &getTable()
    {
        return $this->table;
    }

    private function getMap()
    {
        return $this->map;
    }

    private function setMap($xmlObj)
    {
        $map = array();
        foreach ($xmlObj->columns as $item) {
            $i = 0;
            foreach ($item->children() as $key => $value) {
                if (empty($map[(string)$value]))
                    $map[(string)$value] = (string)$value['type'];
                $i++;
            }
        }
        return $map;
    }

    public function __toString()
    {
        return '<pre>' . print_r($this->table, true) . '</pre>';
    }

    public function query($order = array("id" => "ASC"),
                          $filter = array("[>=]id" => 0),
                          $limit = false,
                          $select = array("*"))
    {
        $this->validateQuery($filter, $order, $limit, $select);
        $query = $this->filter($filter, $this->table);
        $query = $this->order($order, $query);
        $query = $this->limit($limit, $query);
        $query = $this->select($select, $query);
        $this->printTable($select, $query);
        return $query;
    }

    private function validateQuery(&$filter, &$order, &$limit, &$select)
    {
        if (empty($filter) || is_null($filter))
            $filter = array("[>=]id" => 0);
        if (empty($order) || is_null($order))
            $order = array("id" => "ASC");
        if (empty($limit) || is_null($limit))
            $limit = false;
        if (empty($select) || is_null($select))
            $select = array("*");
    }

    private function printTable($select, $query)
    {
        echo '<table class="table_blur" align="center">';
        if (in_array('*', $select))
            $select = array_keys($this->map);
        foreach ($select as $value)
            echo '<th>' . $value . '</th>';
        foreach ($query as $key => $value) {
            echo '<tr>';
            foreach ($value as $k => $v) {
                echo '<td>' . $value[$k] . '</td>';
            }
            echo '</tr>';
        }
        echo '</table>';
    }

    public function delete($id)
    {
        $table = &$this->getTable();
        unset($table[$id]);
    }

    public function update($id, $params)
    {
        $table = &$this->getTable();
        foreach ($params as $key => $value) {
            if (isset($this->map[$key]))
                $table[$id][$key] = $value;
        }
    }

    public function add($params)
    {

        $table = &$this->getTable();
        end($table);
        $last = key($table) + 1;
        reset($table);

        $table[$last] = array();
        $map = $this->getMap();
        foreach ($map as $key => $value) {
            if (!isset($params[$key])) {
                $table[$last][$key] = '';
                if ($value == 'date')
                    $table[$last][$key] = date('d M Y');
            } else {
                $table[$last][$key] = $params[$key];
            }
        }
        if (isset($this->map['id']))
            $table[$last]['id'] = $last;
    }

    private function filter($filter, $table)
    {
        $result = array();
        foreach ($table as $key => $value) {
            if ($this->modificator($filter, $value)) {
                $value[$this->map['date']] = date('d M Y', strtotime($value[$this->map['date']]));
                $result[$key] = $value;
            }
        }
        return $result;
    }

    private function modificator($filter, $item)
    {
        $flag = true;
        foreach ($filter as $key => $value) {
            $modificator = array(substr($key, stripos($key, "[") + 1, strpos($key, "]") - 1));
            $keyItem = substr($key, stripos($key, "]") + 1, strlen($key));
            if (count($modificators = explode("|", $modificator[0])) > 1)
                $modificator = $modificators;
            foreach ($modificator as $mod) {
                if ($this->map[$keyItem] == "num") {
//                    var_dump($mod);
//                    echo "<br>";
                    $flag = $flag && $this->modificatorNumeric($mod, $item, $keyItem, $value);
                } elseif ($this->map[$keyItem] == "string") {
                    $flag = $this->modificatorString($mod, $flag, $item, $keyItem, $value);
                } elseif ($this->map[$keyItem] == "date") {
                    $flag = $this->modificatorDate($mod, $flag, $item, $keyItem, $value);
                }
            }
        }
        return in_array("!", $modificators) ? !$flag : $flag;
    }

    private function modificatorNumeric($mod, $item, $keyItem, $value)
    {
//        var_dump($mod);
//        echo "<br>";
        switch ($mod) {
            case ">": {
                return $item[$keyItem] > $value;
            }
            case ">=": {
                return  $item[$keyItem] >= $value;
            }
            case "<": {
                return $item[$keyItem] < $value;
            }
            case "<=": {
                return $item[$keyItem] <= $value;
            }
            case "><": {
                return $item[$keyItem] >= $value[0] && $item[$keyItem] <= $value[1];
            }
            default:
                return $item[$keyItem] == $value;
        }
    }

    private function modificatorString($mod, $flag, $item, $keyItem, $value)
    {
        switch ($mod) {
            case "cont": {
                $flag = $flag && strpos($item[$keyItem], $value);
                break;
            }
            default:
                $flag = $flag && strcasecmp($item[$keyItem], $value);
                break;
        }
        return $flag;
    }

    private function modificatorDate($mod, $flag, $item, $keyItem, $value)
    {
        $item[$keyItem] = strtotime($item[$keyItem]);
        if (is_array($value)) {
            $value[0] = strtotime($value[0]);
            $value[1] = strtotime($value[1]);
        } else
            $value = strtotime($value);
        switch ($mod) {
            case ">": {
                $flag = $flag && $item[$keyItem] > $value;
                break;
            }
            case ">=": {
                $flag = $flag && $item[$keyItem] >= $value;
                break;
            }
            case "<": {
                $flag = $flag && $item[$keyItem] < $value;
                break;
            }
            case "<=": {
                $flag = $flag && $item[$keyItem] <= $value;
                break;
            }
            case "><": {
                $flag = $flag && $item[$keyItem] >= $value[0] && $item[$keyItem] <= $value[1];
                break;
            }
            default:
                $flag = $flag && $item[$keyItem] == $value;
                break;
        }
        return $flag;
    }

    private function array_msort($array, $cols) //todo метод для сортировки
    {
        $colarr = array();
        foreach ($cols as $col => $order) {
            $colarr[$col] = array();
            foreach ($array as $k => $row) {
                $colarr[$col]['_' . $k] = strtolower($row[$col]);
            }
        }
        $eval = 'array_multisort(';
        foreach ($cols as $col => $order) {
            $eval .= '$colarr[\'' . $col . '\'],' . $order . ',';
        }
        $eval = substr($eval, 0, -1) . ');';
        eval($eval);
        $ret = array();
        foreach ($colarr as $col => $arr) {
            foreach ($arr as $k => $v) {
                $k = substr($k, 1);
                if (!isset($ret[$k])) $ret[$k] = $array[$k];
                $ret[$k][$col] = $array[$k][$col];
            }
        }
        return $ret;
    }

    private function order($condition, $table)
    {
        $date = false;
        $myKey = "";
        foreach ($condition as $key => &$value) {
            if (strtolower($value) == "asc")
                $value = SORT_ASC;
            else
                $value = SORT_DESC;
            if ($this->map[$key] == "date") {
                $date = true;
                $myKey = $key;
                foreach ($table as $k => &$val)
                    $val[$key] = strtotime($val[$key]);
            }
        }
        $table = $this->array_msort($table, $condition);
        if ($date) {
            foreach ($table as $k => &$val)
                $val[$myKey] = date('d M Y', $val[$myKey]);
        }
        return $table;
    }

    private function limit($val, $table)
    {
        if (!$val)
            return $table;
        $result = array();
        $i = 1;
        foreach ($table as $key => $value) {
            if ($i > $val)
                break;
            else {
                $result[$key] = $value;
                $i++;
            }
        }
        return $result;
    }

    private function select($colums, $table)
    {
        $result = array();
        foreach ($colums as $col) {
            if ($col == "*") {
                return $table;
            }
            foreach ($table as $id => $value) {
                foreach ($value as $key => $val) {
                    if (strtolower($key) == strtolower($col))
                        $result[$id][$key] = $val;
                }
            }
        }
        return $result;
    }

}




